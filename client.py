from __future__ import absolute_import, unicode_literals

from datetime import datetime

from requests import RequestException
from zeep import Client, Settings
from zeep.exceptions import Error as ZeepError
from zeep.helpers import serialize_object

from cse_exceptions import CseClientApiError, ElementBuildError, CseResponseValidationError


class CseClient(object):
    login = ''
    password = ''
    debug_mode = False
    client = None
    type_factory = None
    service = None

    def __init__(self, login, password):
        if login and password:
            self.debug_mode = False
            self.login = login
            self.password = password
        else:
            self.login = 'test'
            self.password = '2016'
            self.debug_mode = True

        if self.debug_mode:
            api_url = 'http://lk-test.cse.ru/1c/ws/web1c.1cws?wsdl'
        else:
            api_url = 'http://web.cse.ru/1c/ws/Web1C.1cws?wsdl'
        self.client = Client(api_url, settings=Settings(strict=False))

    def _auth_params(self, payload):
        """
        Метод добавляет данные для авторизации в параметры запроса
        :param payload: Параметры запроса
        :type payload: dict
        :return: Парметры запроса с учетными данными
        :rtype: dict
        """
        payload.update({'login': self.login, 'password': self.password})
        return payload

    def _validate_response(self, response):
        """
        Метод проверяет тело ответа на предмет ключа Error. Если в ответе найдена ошибка, то будет брошено исключение
        CseResponseValidationError, при этом будет попытка получить расшифровку ошибки.
        :param response: Ответ от сервера в виде dict
        :type response: dict
        :return: Метод ничего не возвращает
        """
        errors = []
        try:
            if response['Properties'][0]['Key'] == 'Error':
                error_info = response['Properties'][0]
            else:
                return
        except (IndexError, KeyError):
            return

        try:
            for error_item in error_info['List']:
                errors.append(self.errors_messages.get(error_item['Value'], 'Неизвестная ошибка'))
        except KeyError:
            errors.append('Неизвестная ошибка')
        raise CseResponseValidationError(errors)

    @property
    def errors_messages(self):
        """
        Свойство предназначено для получения расшифровок ошибок, получение ошибок осуществляется через api КСЭ,
        причем запрос так же требует авторизации. При любом падении клиента или неправильной структуре ответа,
        отдается описание ошибки авторизации, таким образом в методах которые будут использовать это свойство можно
        будет определить случай неправильной авторизации и отдать корректный текст ошибки.

        :return: Словарь с описанием ошибок, где ключ это код ошибки, а значение ее описание
        :rtype: dict
        """
        errors = {}
        reference_key = 'ErrorCodes'
        try:
            reference = {'Key': 'Reference', 'Value': reference_key, 'ValueType': 'string'}
            parameters = {'Key': 'parameters', 'List': [reference]}
            params = self._auth_params({'parameters': parameters})
            response = serialize_object(self.client.service.GetReferenceData(**params))
            for error in response['List']:
                errors[error['Key']] = error['Value']
            return errors
        except (ZeepError, KeyError, RequestException):
            return {'03010': u'Ошибка авторизации доступа к базе'}

    @staticmethod
    def build_element(key, value=None, list_value=None, fields=None, properties=None, tables=None):
        """
        Данный метод позволяет создать словарь, который будет соответсвовать структуре Element системы КСЭ.
        Для системы значение (value)
        :param key: Значение для свойства Key
        :param value: Значение для свойства Value
        :param list_value: Значение для свойства List
        :param fields: Значение для свойства Fields
        :param properties: Значение для свойства Properties
        :param tables: Значение для свойства Tables
        :type key: str
        :type value: bool | str | float | datetime | int | None
        :type list_value: list | None
        :type fields: list | None
        :type properties: list | None
        :type tables: list | None
        :return: словарь, который будет соответсвовать структуре Element, словарь можно передавать в запрос
        :rtype: dict
        """
        element = {'Key': key}
        if value:
            types = {bool: 'boolean', str: 'string', float: 'float', datetime: 'dateTime', int: 'float'}

            try:
                value_type = types[type(value)]
            except KeyError:
                raise ElementBuildError(
                    'Недопустимый тип Value {}, допустимые типы - {}'.format(type(value), ', '.join(str(types.keys())))
                )
            if isinstance(value, datetime):
                value = value.isoformat()
            element.update({'Value': value, 'ValueType': value_type})
        if isinstance(list_value, list):
            element.update({'List': list_value})
        if isinstance(fields, list):
            element.update({'Fields': fields})
        if isinstance(properties, list):
            element.update({'Properties': properties})
        if isinstance(tables, list):
            element.update({'Tables': tables})
        return element

    def request(self, service, params):
        """
        Метод request делает запрос к серверу КСЭ, перед запросом params дополняются параметрами авторизации,
        после запроса происходит проверка ответа на ошибки, если ошибка будет найдена, то в итоге будет
        брошено исключение CseClientApiError которое будет содержать текст ошибки.
        :param service: Имя сервиса
        :param params: Парметры запроса
        :type service: str
        :type params: dict
        :rtype: dict
        :return: Ответ от сервера КСЭ
        """
        try:
            params_with_auth = self._auth_params(params)
            response = serialize_object(self.client.service[service](**params_with_auth), dict)
            self._validate_response(response)
            return response
        except (ZeepError, KeyError, TypeError, CseResponseValidationError) as e:
            raise CseClientApiError(str(e))
