
class CseError(Exception):
    pass


class CseResponseValidationError(CseError):

    def __init__(self, errors):
        message = 'При валидации ответа произошли следующие ошибки - {}'.format(', '.join(errors))
        super(CseResponseValidationError, self).__init__(message)


class CseClientApiError(CseError):
    pass


class ElementBuildError(CseError):
    pass
