import datetime
from pprint import pprint

from api import CseApi

if __name__ == "__main__":
    api = CseApi()
    order_detail = {
        'TakeDate': datetime.datetime.now() + datetime.timedelta(days=5),
        'Sender': 'ООО Солнечный свет',
        'SenderGeography': 'cf862f56-442d-11dc-9497-0015170f8c09',
        'SenderAddress': 'г. Москва',
        'SenderPhone': '+79536819812',
        'Recipient': 'Семен Семенович',
        'RecipientGeography': 'cf862f56-442d-11dc-9497-0015170f8c09',
        'RecipientAddress': 'Семеновская 25',
        'RecipientPhone': '89524856821',
        'Urgency': '18c4f208-458b-11dc-9497-0015170f8c09',
        'Payer': 2,
        'PaymentMethod': 1,
        'ShippingMethod': 'e45b6d73-fd62-44da-82a6-44eb4d1d9490',
        'TypeOfCargo': '4aab1fc6-fc2b-473a-8728-58bcd4ff79ba',
        'Weight': 1.5,
        'CargoPackageQty': 1
    }
    result = api.create_order(details=order_detail)
    pprint(result)
