from __future__ import absolute_import, unicode_literals

from client import CseClient
from cse_exceptions import CseClientApiError


class CseApi(object):
    api_client_class = CseClient

    def __init__(self, login=None, password=None):
        self.api_client = self.api_client_class(login, password)

    def currencies(self):
        """
        Метод для получения информации о валютах.
        :return: Словарь, ключ словаря GUID валюты, значение - это словарь с информацией о валюте. Пример:
        {
            '0fa27cad-0b04-11e3-98e9-001e670c42dc': {
                'Code': 'ZAR',
                'Default': False,  # Признак валюты, используемой по умолчанию
                'FullName': 'Рэнд', # Полное наименование валюты
                'IsFolder': False,
                'Level': Decimal('0'),
                'Parent': None
            },
            '84a60e1d-0b02-11e3-98e9-001e670c42dc': {
                'Code': 'AUD',
                'Default': False,
                'FullName': 'Австралийский доллар',
                'IsFolder': False,
                'Level': Decimal('0'),
                'Parent': None
            },
            ...
        }
        :rtype: dict
        """
        result = {}
        reference = self.api_client.build_element('Reference', 'Currencies')
        parameters = self.api_client.build_element('parameters', list_value=[reference])
        response = self.api_client.request('GetReferenceData', {'parameters': parameters})
        try:
            for currency_data in response['List']:
                currency_uuid = currency_data['Key']
                result[currency_uuid] = {'Code': currency_data['Value']}
                for field in currency_data['Fields']:
                    result[currency_uuid].update({field['Key']: field['Value']})
            return result
        except KeyError:
            raise CseClientApiError('Неудалось получить ниформацию о валютах - ошибка обработки ответа')

    def create_order(self, details, description_data=None, client_number=None, agent_number=None,
                     store_depends_on_destination=False):
        """
        Метод для создания заказов
        :param details: Реквизиты документа, в виде словаря
                ContactPerson                   (str) GUID контактного лица
                Department                      (str) GUID подразделения, или подразделение в виде наименования
                Project                         (str) GUID проекта
                TakeDate                        (datetime, обязательный элемент) Дата забора груза(дата и время).
                TakeTime                        (str) Время забора груза в формате 00:00 00:00
                TakeAtOffice                    (bool) Груз будет принят для доставки в офисе –по умолчанию равно истине
                                                    для всех заказов с товарной составляющей (заказов на склад),
                                                    ложь для остальных заказов (то есть будет послан курьер для забора)
                DeliveryDate                    (datetime) Желательная  дата  доставки  груза. Дата доставки должна быть
                                                        больше даты забора и времени создания документа.
                DeliveryTime                    (str) Желательное  время  доставки  груза  в свободном формате
                Comment                         (str) Общий комментарий к заказу
                Sender                          (str, обязательный элемент) Отправитель груза
                SenderOfficial                  (str) Контактное лицо отправителя груза
                SenderGeography                 (str, обязательный элемент) GUID географии отправления, или код
                                                    географии клиента, если клиент пользуется собственной географией.
                                                    Либо код ФИАС, но для этого необходимо передать строку «fias-» и
                                                    необходисмый код ФИАС. Для поиска по индексу: «postcode-» и индекс
                                                    для поиска (данный вариант поиска может возвращать не точную
                                                    информацию)
                SenderAddress                   (str, обязательный элемент) Адрес забора груза
                SenderPhone                     (str, обязательный элемент) Контактный телефон отправителя
                SenderEMail                     (str) Адрес электронной почты отправителя
                SenderInfo                      (str) Дополнительная информация по отправителю
                SenderPVZ                       (str) GUID пунктавыдачи заказов. Если указан, то адрес и география
                                                    берутся из элемента ПВЗ
                Recipient                       (str, обязательный элемент) Получатель груза
                RecipientOfficial               (str) Контактное лицо получателя груза
                RecipientGeography              (str, обязательный элемент) GUID географии доставки, или код географии
                                                    клиента, если  клиент  пользуется  собственной географией.
                                                    Либо код ФИАС, но для этого необходимо передать строку «fias-» и
                                                    необходисмый код ФИАС. Для поиска по индексу: «postcode-» и индекс
                                                    для поиска (данный вариант поиска может возвращать не точную
                                                    информацию)
                RecipientAddress                (str, обязательный элемент) Адрес доставки груза
                RecipientPhone                  (str, обязательный элемент) Контактный телефон получателя
                RecipientEMail                  (str) Адрес электронной почты получателя
                RecipientInfo                   (str) Дополнительная информация по получателю
                RecipientPVZ                    (str) GUID пункта выдачи заказов. Если указан, то адрес и география
                                                        берутся из элемента ПВЗ
                Repository                      (str) GUID склада
                Urgency                         (str, обязательный элемент) GUID срочности доставки
                Payer                           (int, обязательный элемент) Код плательщика
                PaymentMethod                   (int, обязательный элемент) Код способа оплаты
                ShippingMethod                  (str, обязательный элемент) GUID способа доставки
                TypeOfCargo                     (str, обязательный элемент) GUID вида груза
                WithReturn                      (bool) Признак того, что после доставки необходимо вернуть
                                                    груз (документы) отправителю
                Weight                          (float, обязательный элемент) Вес груза в кг.
                Height                          (float) Высота груза в см
                Length                          (float) Длина груза в см.
                Width                           (float) Ширина груза в см.
                CargoDescription                (str) Описание груза
                CargoPackageQty                 (int, обязательный элемент) Количество мест, целое неотрицательное число
                InsuranceRate                   (float) Страховая стоимость груза
                InsuranceRateCurrency           (str) GUID валюты страховой стоимости  груза если не указано,
                                                    то валюта –рубли
                DeclaredValueRate               (float) Заявленная стоимость груза
                DeclaredValueRateCurrency       (str) GUID валюты заявленной стоимости груза, если не указано,
                                                    то валюта –рубли
                ValueForCustomsPurposes         (float) Таможенная стоимость груза
                ValueForCustomsPurposesCurrency (string) GUID валюты таможенной стоимости груза, если не указано,
                                                    то валюта –рубли
                ItemsProcessingAction           (string) Действие обработки товара (имеет смысл при указании товарной
                                                    составляющей). Может иметь одно из значений: «inventory»
                                                    (инвентаризация), «assembly» (комплектация), «shipping» (отгрузка),
                                                    «selection» (подбор) и «incoming» (приход)
                ReplyEMail                      (string) Адрес электронной почты для отправки уведомлений о состоянии
                                                    доставки
                ReplySMSPhone                   (string) Телефон  для  отправки  уведомлений SMSо состоянии доставки
                IsDistribution                  (bool) Признак что заказ является рассылкой и данные получателя не з
                                                    аполняются.
        :param description_data: Словарь с дополнительной информацией о заказе, ключ - Название описания (CargoPackages,
                                Items, ClientCodes, Waybills), значение - список словарей с описанными ниже элементами.
                CargoPackages - описание характеристик составного груза
                    Структура элементов:
                        PackageID     (string) Номер  упаковки  (если  указан,  то  проверяется  на уникальность)
                        Weight        (float) Вес груза в кг.
                        Height        (float) Высота груза в см.
                        Length        (float) Длина груза в см.
                        Width         (float) Ширина груза в см.
                        PackageQty    (float) Количество мест, целое неотрицательное число
                Items - Описание сведений о списке отгружаемых товаров
                    Структура элементов
                        Item          (string, обязательно) GUID товара
                        Party         (string) GUID партии товаров
                        SerialNumber  (string) GUID серийного номера товара
                        Package       (string) GUID единицы измерения, упаковки
                        VATRate       (float) Код ставки НДС
                        Qty           (float, обязательно) Количество, целое неотрицательное число
                        Price         (float) Цена
                        Barcode       (string) Штрихкод производителя
                        Comment       (string) Комментарий к товару
                        AssessedValue (float) Оценочная стоимость товара (руб)
                ClientCodes - Данные о дополнительных кодах
                    Структура элементов
                        ClientCodeID  (string, обязательно) GUID дополнительного номера
                        ClientCode    (string, обязательно) Значение дополнительного кода
                Waybills - номера ранее созданных накладных для создания общего закза на забор грузов
                    Структура элементов
                        Number        (string, обязательно) Номер накладной
                        NumberType    (string) Тип  номера  для  поиска.  По  умолчанию  номер накладной КСЭ
        :param client_number: Клиентский номер сохранённого документа (если есть)
        :param agent_number: Номер агента сохранённого документа (если есть)
        :param store_depends_on_destination: Признак того, что при создании заказа с товарной составляющей для
                        интернет-магазина определять склад отгрузки через географию доставки (из ближайшего офиса),
                        в противном случае выбирать основной склад товаров клиента
        :return: Информация о созданном заказе в виде словаря
            Number        (str) Номер сохранённого в базе документа
            ClientNumber  (str|None) Клиентский  номер  сохранённого  документа  (если есть)
            AgentNumber   (str|None) Агентский  номер  сохранённого  документа  (если есть)
            CreateDate    (datetime) Дата и время созданного документа
            DeliveryDate  (datetime|None) Планируемая дата доставки
        :rtype dict
        """

        parameters = self.api_client.build_element('parameters', list_value=[
            self.api_client.build_element('DocumentType', 'Orders'),
            self.api_client.build_element('StoreDependsOnDestination', store_depends_on_destination),
        ])

        properties = []
        if client_number:
            properties.append(self.api_client.build_element('ClientNumber', client_number))
        if agent_number:
            properties.append(self.api_client.build_element('AgentNumber', agent_number))

        details_elements = []
        for detail_name, detail_value in details.items():
            details_elements.append(self.api_client.build_element(detail_name, detail_value))

        tables = []
        if description_data:
            for table_name, rows in description_data.items():

                rows_elements = []
                for row_name, row_value in rows.items():
                    rows_elements.append(self.api_client.build_element(row_name, row_value))

                table = self.api_client.build_element(table_name, fields=rows_elements)
                tables.append(table)

        if not properties:
            properties = None
        if not tables:
            tables = None
        order = self.api_client.build_element('Order', properties=properties, tables=tables, fields=details_elements)
        data = self.api_client.build_element('Orders', list_value=[order])
        response = self.api_client.request('SaveDocuments', {'parameters': parameters, 'data': data})
        try:
            result = {'Number': None, 'ClientNumber': None, 'AgentNumber': None, 'CreateDate': None,
                      'DeliveryDate': None}
            for prop in response['List'][0]['Properties']:
                result.update({prop['Key']: prop['Value']})
            return result
        except (KeyError, IndexError):
            raise CseClientApiError('Неудалось получить ниформацию о созданных заказах - ошибка обработки ответа')
